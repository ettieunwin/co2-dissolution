'''
1D analytical model for averaged concentration profile
H. J. T. Unwin
May 2015

For a 1D domain \Omega of length 100,
solves:
dc/dx + \alpha d^2c/dx^2 + d/dx(dc/dx)^3 = 0 on \Omega
    c(0) = 1
    c(100) = 0

where \alpha = (120/(Ra^4 u_B^2))^(1/3)
'''

from dolfin import *
import matplotlib.pyplot as plt
import numpy as np

Ras = [10000]
u_B = 8.0E-4

# Sub domain for Dirichlet boundary condition
class Left(SubDomain):
    def inside(self, x, on_boundary):
        return x[0] < DOLFIN_EPS and on_boundary
class Right(SubDomain):
    def inside(self, x, on_boundary):
        return x[0] == 100.0 and on_boundary

# Create mesh and define function space
n = 50000
mesh = IntervalMesh(n, 0.0, 100.0)
V = FunctionSpace(mesh, "CG", 1)

# Define boundary condition
bc1 = DirichletBC(V, 1.0, Left())
bc2 = DirichletBC(V, 0.0, Right())
bcs = [bc1, bc2]

# Define variational problem
c = Function(V)
v = TestFunction(V)

for Ra in Ras:

    # Defines equations for each Ra
    alpha = (120.0/(u_B**2*Ra**4))**(1.0/3.0)
    F = v*c.dx(0)*dx - alpha*c.dx(0)*v.dx(0)*dx - v.dx(0)*(c.dx(0))**3*dx

    # Compute solution
    solve(F == 0, c, bcs, solver_parameters={"newton_solver":
                                          {"maximum_iterations": 100}})

    # Identifies c and x values
    c_vals = c.vector().array()[::-1]
    x_vals = np.linspace(0.0, 100.0, n+1)
    x_vals = x_vals*(2.0/3.0)

    # Plot solution and solution gradient
    plt.plot(x_vals, c_vals, label=r"$\alpha = {0:.4f}$".format(alpha))

# Labels plot
plt.xlim((0, 4))
plt.xlabel(r"$x/X_{dis}$")
plt.ylabel(r"$\bar{c}$")
plt.legend()
plt.rc('text', usetex=True)
plt.show()
