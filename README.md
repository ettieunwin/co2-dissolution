#CO~2~ dissolution in a background hydrological flow, H. J. T Unwin, G. N. Wells and A. W. Woods supporting material

This supporting material contains the two solvers that were used in the above
paper.

* 1d_model.py - This is the one dimensional solver of the vertically averaged transport equation (3.15).

* solver_paper.py - This is the solver for the coupled Darcy flow and transport equations in two dimensions equations (2.7-2.17).

More information about the finite element method used can be found in finite_element_formulations.pdf.

To produce the data for the figures in the paper, the following values of u~B~ (the background flow speed) and Ra (the Rayleigh number equation (2.18)) were input into solver_paper.py:

* Figure 2
	* u~B~ = 8 $\times$ 10^-4^ and various Ra values
	* u~B~ = 8 $\times$ 10^-3^ and various Ra values
	
	
* Figure 4
	* u~B~ = 0.1 and Ra = 3000
	* u~B~ = 0.1 and Ra = 50
	* u~B~ = 0.1 and Ra = 1
	
	
* Figure 5
	* u~B~ = 8 $\times$ 10^-4^ and Ra = 10000
	* u~B~ = 8 $\times$ 10^-4^ and Ra = 1000
	* u~B~ = 8 $\times$ 10^-4^ and Ra = 500
	* u~B~ = 8 $\times$ 10^-4^ and Ra = 175
	* u~B~ = 8 $\times$ 10^-4^ and Ra = 100

* Figure 6
	* u~B~ = 0.1 and Ra = 3000

* Figure 7a
	* u~B~ = 8 $\times$ 10^-4^ and Ra = 1000
	
* Figure 7b
	* u~B~ = 8 $\times$ 10^-4^ and Ra = 10000
	* u~B~ = 8 $\times$ 10^-4^ and Ra = 1000
	* u~B~ = 8 $\times$ 10^-4^ and Ra = 750
	* u~B~ = 8 $\times$ 10^-4^ and Ra = 500
	* u~B~ = 8 $\times$ 10^-4^ and Ra = 250
	* u~B~ = 8 $\times$ 10^-4^ and Ra = 100
	
* Figure 9
	* u~B~ = 1.0 and Ra = 1000
	* u~B~ = 1.0 and Ra = 2
	* u~B~ = 1.0 and Ra = 0.5
	
* Figure 10 
	* Ra = 10 and u~B~ = 0.05, 0.25, 0.5, 0.75, 1.0, 1.25, 1.5, 2
	* Ra = 1.0 and u~B~ = 0.05, 0.25, 0.5, 0.75, 1.0, 1.25, 1.5, 2
	* Ra = 0.1 and u~B~ = 0.05, 0.25, 0.5, 0.75, 1.0, 1.25, 1.5, 2
	* Ra = 0.01 and u~B~ = 0.05, 0.25, 0.5, 0.75, 1.0, 1.25, 1.5, 2
	
* Figure 11
	* u~B~ = 1.0 and Ra = 10
	* u~B~ = 1.0 and Ra = 1.0
	* u~B~ = 1.0 and Ra = 0.1

For u~B~ = 8 $\times$ 10^-4^ the more refined mesh (mesh_1_100_ref.xml) was used, whereas for the other simulations mesh_1_100.xml was used.
