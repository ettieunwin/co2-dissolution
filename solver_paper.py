'''
Modelling CO_2 Sequestation in an Aquifer
H. J. T Unwin and G. N. Wells
May 2015

For a domain \Omega:={(x,y) \in (0,100) x (0,1)}
solves the non dimensional continuity equation and Darcy
flow equation using a mixed method and then the advection
diffusion equation:

div(u) = 0            on \Omega x I,
u = -grad(p) + c*e_k  on \Omega x I,
p = p_D               on \Gamma_{c} x I,
u.n = -ub             on \Gamma_{B} x I
u.n = 0               on \Gamma_{cap} x I,

      where p_D = -c_{D}*x[1]/H

dc/dt + grad(c).u - (1/Ra)*grad^2(c) = 0   on \Omega x I,
cu.m = min(u.n,0)c_{D}                     on \Gamma_{c} x I,
-grad(c).n = 0                             on \Gamma_{c} x I,
(-(1/Ra)grad(c) + c*u).n = 0               on \Gamma_{B} x I,
(-(1/Ra)grad(c_ + c*u).n = 0               on \Gamma_{cap} x I,
c(x,0) = c_0                               on \Omega,

      where c_D = 1,
            c_0 = 0.
'''

from dolfin import *

class VelocitySolver:
    def __init__(self, mesh, bcs_m, f, dss, dt):
        self.f = f
        self.dt = dt
        self.u_bc = [bcs_m[1], bcs_m[2]]
        self.mesh = mesh

        n = FacetNormal(self.mesh)

        # Mixed form
        self.BDM = FunctionSpace(self.mesh, "BDM", 1)
        self.DG = FunctionSpace(self.mesh, "DG", 0)
        self.W = self.BDM * self.DG

        u, p = TrialFunctions(self.W)
        sigma, v = TestFunctions(self.W)

        F0 = dot(sigma, u)*dx - (p*div(sigma))*dx \
           - (dot(sigma, self.f))*dx \
           + (dot(sigma, n)*bcs_m[0])*dss(1)
        F1 = v*(div(u))*dx
        F_m = F0 + F1
        self.a_m, self.L_m = lhs(F_m), rhs(F_m)
        self.w_m = Function(self.W)

    def u(self):
        A = assemble(self.a_m)
        b = assemble(self.L_m)

        for bc in self.u_bc:
            bc.apply(A, b)

        solve(A, self.w_m.vector(), b)
        self.u_m, self.p_m = self.w_m.split()
        return self.u_m


class ConcentrationSolver:
    def __init__(self, mesh, u, c0, c_bc, Ra, dt, dss):
        self.c0 = c0
        self.dt = dt
        self.u = u
        self.c_bc = c_bc
        self.Ra = Ra
        self.alpha = 5.0

        self.DG1 = FunctionSpace(mesh, "DG", 1)

        n = FacetNormal(mesh)
        h = CellSize(mesh)

        # Upwind velocity
        un = (dot(u, n) + abs(dot(u, n)))/2.0
        ud = (dot(u, n) - abs(dot(u, n)))/2.0

        c, v = TrialFunction(self.DG1), TestFunction(self.DG1)
        theta = 1.0
        c_mid = (1.0-theta)*c0 + theta*c

        F_a = - c_mid*inner(grad(v), u)*dx \
              + inner(jump(v), un('+') \
              * c_mid('+')- un('-')*c_mid('-'))*dS\
              + v*c_mid*un*dss(1) \
              + v*c_bc*ud*dss(1)

        F_d = inner(grad(v),(1.0/self.Ra)*grad(c_mid))*dx \
              + (1.0/self.Ra)*(self.alpha/h('+'))\
              * dot(jump(v,n),jump(c_mid, n))*dS \
              - (1.0/self.Ra)*dot(avg(grad(v)),jump(c_mid,n))*dS \
              - (1.0/self.Ra)*dot(jump(v,n),avg(grad(c_mid)))*dS
        F = v*(c-c0)*dx + dt*F_d + dt*F_a

        self.a, self.L = lhs(F), rhs(F)
        self.c = Function(self.DG1)

    def solve(self):
        # Solve
        solve(self.a==self.L, self.c)
        return self.c


# Create an application parameter set
application_parameters = Parameters("application_parameters")

# Create default application parameters
application_parameters.add("Ra",  1000.0)
application_parameters.add("ub",   8.0E-4)
application_parameters.add("nsteps", 100)
application_parameters.add("dt",     1.0)
application_parameters.add("no",       1)

application_parameters.add("no_cp", 1000)
application_parameters.add("start", 0)

# Parse command line parameters
application_parameters.parse()
Ra = application_parameters["Ra"]
ub = application_parameters["ub"]
no = application_parameters["no"]

no_cp = application_parameters["no_cp"]
start = application_parameters["start"]

parameters["ghost_mode"] = "shared_facet"

# Creates output files
if start == 0:
    test_file_u = File("results_"+str(no)+"/data/u.xdmf")
    test_file_c = File("results_"+str(no)+"/data/c.xdmf")
else:
    test_file_u = File("results_"+str(no)+"/data/u_"+str(start)+".xdmf")
    test_file_c = File("results_"+str(no)+"/data/c_"+str(start)+".xdmf")

def solve_problem(p_m_bcs, f, c0, c_bc, dss, num_steps, dt):
    # Create velocity solver
    velocity_solver = VelocitySolver(mesh, p_m_bcs, f, dss, dt)

    # Create transport solver
    concentration_solver = ConcentrationSolver(mesh, velocity_solver.u(),
                                               c0, c_bc, Ra, dt, dss)

    counter = 0
    # Step in time
    for step in range(start,  num_steps+1):

        print("Step number: {}".format(step))

        # Compute the velocity field
        u = velocity_solver.u()
        test_file_u << (u , float(step))

        # Ramp up boundary concentration
        if step < 20:
            c_bc.ramp = step/20.0
        else:
            c_bc.ramp = 1.0

        # Compute scalar concentration
        c0.assign(concentration_solver.solve())
        test_file_c << (c0, float(step))

        # Checkpoints the concentration
        if counter == int(no_cp):
            File("results_"+str(no)+"/data/snapshot/c_"+str(step)+".xdmf") << c0
            File("results_"+str(no)+"/data/snapshot/c_"+str(step)+".xml") << c0
            File("results_"+str(no)+"/data/snapshot/u_"+str(step)+".xdmf") << u
            counter = 1
            print("Checkpointing on step {}".format(step))
        else:
            counter += 1

# Model parameters
c_bc = Expression('1.0*ramp', ramp = 0.0)

# Create domain
lx = 100.0
ly = 1.0
mesh = Mesh('mesh_1_100.xml')

# Define boundaries
boundary_parts = FacetFunction("size_t", mesh, 0)

boundary = CompiledSubDomain("on_boundary")
left  = CompiledSubDomain("near(x[0], 0.0)")
right = CompiledSubDomain("near(x[0], lx)", lx=lx)
boundary.mark(boundary_parts,3)
left.mark(boundary_parts,1)
right.mark(boundary_parts,2)
dss = ds(subdomain_data=boundary_parts)

# Create function spaces
V = FunctionSpace(mesh, "Lagrange", 2)
U = VectorFunctionSpace(mesh, "Lagrange", 2)
DG1 = FunctionSpace(mesh, 'DG', 1)
BDM = FunctionSpace(mesh, "BDM", 1)
DG = FunctionSpace(mesh, "DG", 0)
W = BDM * DG

# Initialise boundary conditions
p_left  = Expression('-(1.0/ly)*x[1]',
        ly=ly, element=W.sub(1).ufl_element())
u_right = DirichletBC(W.sub(0), Constant((-ub, 0.0)), boundary_parts, 2)
u_bc = DirichletBC(W.sub(0), Constant((0.0, 0.0)), boundary_parts, 3)
p_m_bcs = [p_left, u_right, u_bc]

# Initialise the concetration field
if start == 0:
    c0 = Function(DG1)
    c0.interpolate(Constant(0.0))
else:
    c0 = Function(DG1, "results_"+str(no)+"/data/snapshot/c_"+str(start)+".xml")


# Create varying density
f = Expression(('0.0', '-c0'), c0=c0, element = U.ufl_element())

# Solve problem
solve_problem(p_m_bcs, f, c0, c_bc, dss,
              num_steps=application_parameters["nsteps"],
              dt=application_parameters["dt"])

# Writes a text file explaining parameters.
if start == 0:
    file = open("results_"+str(no)+"/parameters.txt", "w")
else:
    file = open("results_"+str(no)+"/parameters_"+str(start)+".txt", "w")
file.write('L is ' + str(lx) + '\n')
file.write('d is ' + str(ly) + '\n')
file.write('Ra is ' + str(Ra)+'\n')
file.write('u_b is ' + str(ub)+'\n')
file.write('dt is ' + str(application_parameters["dt"]) + '\n')
file.close()
